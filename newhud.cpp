#include "pch.h"
#include "cube.h"

#define MAXINTERVAL 1000
#define MAXDIST 100

#define LIGHTTIMER (DWORD)3

int m_int = 255;
int m_stay = 255;
string txt;
float thunder = 0.0f;
float Energy = 255;
bool insprint = false;
int zwin = 0;
int hwin = 0;
int nvgoggle = 0;
int flashl = 0;
int init = true;

string buffer;

HUDColor hcl;

int CLFLASH[255];

static Texture *health = NULL;
static Texture *armor = NULL;
static Texture *sprintp = NULL;

static Texture *zm_win = NULL;
static Texture *hm_win = NULL;
static Texture *headshot = NULL;

static Texture *pistol = NULL;
static Texture *grenade = NULL;
static Texture *knife = NULL;
static Texture *subgun = NULL;
static Texture *shotgun = NULL;
static Texture *assault = NULL;
static Texture *sniper = NULL;
static Texture *sg552 = NULL;
static Texture *flare = NULL;
static Texture *m4a1 = NULL;

void InitWE()
{
	if(!health) { health = textureload("packages/misc/customhud/health.png"); }
	if(!armor) { armor = textureload("packages/misc/customhud/armor.png"); }
	if(!sprintp) { sprintp = textureload("packages/misc/customhud/sprint.png"); }

	if(!headshot) { headshot = textureload("packages/misc/customhud/headshot.png"); }
	if(!hm_win) { hm_win = textureload("packages/misc/customhud/humans_win.png"); }
	if(!zm_win) { zm_win = textureload("packages/misc/customhud/zombies_win.png"); }

	if(!pistol) { pistol = textureload("packages/misc/customhud/wpn_pistol.png"); }
	if(!grenade) { grenade = textureload("packages/misc/customhud/wpn_grenade.png"); }
	if(!knife) { knife = textureload("packages/misc/customhud/wpn_knife.png"); }
	if(!subgun) { subgun = textureload("packages/misc/customhud/wpn_subgun.png"); }
	if(!shotgun) { shotgun = textureload("packages/misc/customhud/wpn_shotgun.png"); }
	if(!assault) { assault = textureload("packages/misc/customhud/wpn_assault.png"); }
	if(!sniper) { sniper = textureload("packages/misc/customhud/wpn_sniper.png"); }

	if(!flare) { flare = textureload("packages/misc/customhud/wpn_flare.png"); }
	if(!sg552) { sg552 = textureload("packages/misc/customhud/wpn_sg552.png"); }
	if(!m4a1) { m4a1 = textureload("packages/misc/customhud/wpn_m4a1.png"); }

	if(init)
	{
		init = false;
		//do init stuff here

		hcl.r = 10;
		hcl.g = 10;
		hcl.b = 255;
	}
}

void glRectp(float x,float y,float width,float height)
{
	glBegin(GL_POLYGON);
	glVertex2f(x,y);
	glVertex2f(x + width,y);
	glVertex2f(x + width,y + height);
	glVertex2f(x,y + height);
	glEnd();
}

void glNSRectf(double x, double y, double radius)
{
	double y1=y+radius;
	double x1=x;
	glBegin(GL_LINE_STRIP);
	for(double angle=0.0f;angle<=(2.0f*3.14159);angle+=0.01f)
	{
		double x2=x+(radius*(float)sin((double)angle));
		double y2=y+(radius*(float)cos((double)angle));
		glVertex2d(x1,y1);
		y1=y2;
		x1=x2;
	}
	glEnd();
}

void ShowHumWin()
{
	hwin = 255;
}

void ShowZmbWin()
{
	zwin = 255;
}

void SprintHook()
{

}

void di(Texture *tex, float x, float y, float s, int col, int row, float ts)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    if(tex && tex->xs == tex->ys) quad(tex->id, x, y, s, ts*col, ts*row, ts);
	glDisable(GL_BLEND);
}

void dii(Texture *tex, float x, float y, float s, int col, int row, float ts)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if(tex && tex->xs == tex->ys) quad(tex->id, x, y, s, ts*col, ts*row, ts);
	glDisable(GL_BLEND);
}

DWORD mlStore = 0;

void thundereffect()
{
	int lg;

	if(thunder > 0.01)
	{
		if(LIGHTTIMER - totalmillis > mlStore)
		{
			mlStore = totalmillis;
			thunder -= 0.1;
			lg = abs(sin(thunder)*100);
			fullbrightlight((int)lg);
		}
	}
}

void ZmbMsgs()
{
	if(zwin > 0)
	{
		zwin--;
		glPushMatrix();
		glColor4ub(255,255,255,zwin);
		dii(zm_win,VIRTW/2-500,VIRTH/2-1500,1000,0,0,1);
		glColor4ub(255,255,255,zwin);
		glPopMatrix();
	}

	if(hwin > 0)
	{
		hwin--;
		glPushMatrix();
		glColor4ub(255,255,255,hwin);
		dii(hm_win,VIRTW/2-500,VIRTH/2-1500,1000,0,0,1);
		glColor4ub(255,255,255,hwin);
		glPopMatrix();
	}
}

void WpnCheck()
{
	if(team_int(player1->team) == TEAM_CLA && player1->weaponsel->type != GUN_KNIFE)
	{
		player1->weaponchanging = GUN_KNIFE;
		player1->selectweapon(GUN_KNIFE);
		player1->weaponswitch(player1->weapons[GUN_KNIFE]);
	}
}

DWORD lStore;

void flight(const vec &o,bool scope)
{

		if(player1->weaponsel->type == GUN_SNIPER && scope)
		{
			adddynlight(player1,player1->o,42,100,100,200,10,10);
		}

		if(team_int(player1->team) == TEAM_RVSF && !scope && player1->flashlight && player1->state == CS_ALIVE)
		{
			int flradius;
			int flstrength;

			float fdist = player1->o.dist(o);
		
			flradius = ((fdist/3.5)*1.3)+6;
			flstrength = (100 - fdist*2)*2;

			if(flradius > 29) return;
			if(flstrength < 0) return;
			adddynlight(NULL,o,flradius,1,1,flstrength,flstrength,flstrength);
		}
}

void sprkey(bool on)
{
	insprint = on;
}

DWORD gtcStore[2];

void LightLoop()
{
	thundereffect();

	if(LIGHTTIMER < totalmillis - gtcStore[0])
	{
		gtcStore[0] = totalmillis;

		loopv(bounceents) if(bounceents[i] && bounceents[i]->type == BT_FLARE)
		{
			adddynlight(bounceents[i],bounceents[i]->o,10,30,30,150,10,10);
		}
	}

	if(LIGHTTIMER < totalmillis - gtcStore[1])
	{
		gtcStore[1] = totalmillis;
		WpnCheck();

		if(team_int(player1->team) == TEAM_CLA && player1->state == CS_ALIVE && ZMMode != ZM_NEMESIS)
		{
			adddynlight(player1,player1->o,70,500,500,10,250,10);
		}

		if(team_int(player1->team) == TEAM_CLA && player1->state == CS_ALIVE && ZMMode == ZM_NEMESIS)
		{
			adddynlight(player1,player1->o,15,500,500,255,1,1);
		}

		loopv(players) if(players[i] && players[i]->state == CS_ALIVE && team_int(players[i]->team) == TEAM_RVSF && players[i]->flashlight)
		{
			adddynlight(players[i],players[i]->o,6,500,500,190,190,210);
		}

		loopv(players) if(players[i] && players[i]->state == CS_ALIVE && team_int(players[i]->team) == TEAM_CLA && ZMMode == ZM_NEMESIS)
		{
			adddynlight(players[i],players[i]->o,15,500,500,255,0,0);
		}

		if(team_int(player1->team) == TEAM_CLA) return;

		if(nvgoggle && Energy > 0)
		{
			Energy -= 0.5;
			adddynlight(player1,player1->o,29,50,50,10,190,10);
		} else {
			nvgoggle = 0;
			addmsg(EF_NVG,"ri",0);
		}

		if(insprint && Energy > 0)
		{
			Energy -= 1;
		} else {
			if(Energy < 255 && !nvgoggle)
			{
				Energy += 1;
			}
		}
	}
	
}

void CustomHUD(playerent *p)
{
	float hppercent;
	float appercent;
	float sprintper;
	float lowhp = abs(sin((float)totalmillis/100));
	char mag[3] = "";
	char clip[3] = "";

	InitWE();
	SprintHook();
	
	sprintper = ((float)Energy/255.0)*490.0;

	if(team_int(p->team) == TEAM_CLA)
	{
		if(ZMMode == ZM_NEMESIS)
		{
			hppercent = ((float)p->health/5000.0)*490.0;
		} else if (ZMMode == ZM_FIRSTZOMBIE) {
			hppercent = ((float)p->health/3000.0)*490.0;
		} else {
			hppercent = ((float)p->health/900.0)*490.0;
		}
	} else {
		hppercent = ((float)p->health/100.0)*490.0;
		appercent = ((float)p->armour/100.0)*490.0;
	}

	glPushMatrix();

	glDisable(GL_TEXTURE_2D);

	if(hppercent > 490) hppercent = 490;
	if(appercent > 490) appercent = 490;

	glEnable(GL_BLEND);
	glColor3f(0.3,0.3,0.3);
	glRectp(100,1580,hppercent+10,50.0);
	glColor3f(0.3,0.3,0.3);
	
	if(p->health < 20)
	{
		glColor4f(1.0,0.0,0.0,lowhp);
		glRectp(105,1585,hppercent,40.0);
		glColor4f(1.0,0.5,0.1,lowhp);
	} else { 
		glColor4f(1.0,0.3,0.1,1.0);
		glRectp(105,1585,hppercent,40.0);
		glColor4f(1.0,0.3,0.1,1.0);
	}

	if(p->armour > 0)
	{
		glColor3f(0.3,0.3,0.3);
		glRectp(100,1660,appercent+10,50.0);
		glColor3f(0.3,0.3,0.3);

		glColor4f(0.1,0.3,1.0,1.0);
		glRectp(105,1665,appercent,40.0);
		glColor4f(0.1,0.3,1.0,1.0);
	}

	if(Energy > 1)
	{
		glColor3f(0.3,0.3,0.3);
		glRectp(100,1745,sprintper+10,30.0);
		glColor3f(0.3,0.3,0.3);

		glColor4f(0.9,0.9,0.0,1.0);
		glRectp(105,1750,sprintper,20.0);
		glColor4f(0.9,0.9,0.0,1.0);
	}

	glDisable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);

	glColor4f(1.0,1.0,1.0,1.0);
	dii(health,20,1565,80.0,0,0,1.0);
	glColor4f(1.0,1.0,1.0,1.0);

	glColor4f(1.0,1.0,1.0,1.0);
	dii(armor,20,1650,70.0,0,0,1.0);
	glColor4f(1.0,1.0,1.0,1.0);

	glColor4f(1.0,1.0,1.0,1.0);
	di(sprintp,20,1740,50.0,0,0,1.0);
	glColor4f(1.0,1.0,1.0,1.0);

	/*hStay -= 0.4;

	if(hStay > 0)
	{
		glEnable(GL_BLEND);
		glColor4f(1.0,1.0,1.0,hStay/255.0);
		dii(headshot,VIRTW/3-50,VIRTH/2-800,900.0,0,0,1.0);
		glDisable(GL_BLEND);
	}*/

	glPushMatrix();
	glEnable(GL_BLEND);

	ZmbMsgs();

	if(team_int(p->team) == TEAM_CLA) return;

	sprintf(mag,"%d",p->weaponsel->mag);
	sprintf(clip,"%d",p->weaponsel->ammo);
	if(p->weaponsel->mag == 0 && p->weaponsel->ammo == 0)
	{
		switch(p->weaponsel->type)
		{
		case GUN_GRENADE: draw_text((const char*)mag,(VIRTW - 120),1721,255,0,0,lowhp*255);break;
		case GUN_FLARE: draw_text((const char*)mag,(VIRTW - 120),1721,255,0,0,lowhp*255);break;
		case GUN_KNIFE: break;
		default: {
			draw_text((const char*)mag,(VIRTW - 282),1721,50,128,255*lowhp);
			draw_text("|",(VIRTW - 100),1718,hcl.r,hcl.g,hcl.b);
			draw_text((const char*)clip,(VIRTW - 170),1721,50,128,255*lowhp); }
		}
	} else {
		switch(p->weaponsel->type)
		{
		case GUN_GRENADE: draw_text((const char*)mag,(VIRTW - 120),1721,hcl.r,hcl.g,hcl.b);break;
		case GUN_FLARE: draw_text((const char*)mag,(VIRTW - 120),1721,hcl.r,hcl.g,hcl.b);break;
		case GUN_KNIFE: break;
		default: {
			draw_text((const char*)mag,(VIRTW - 282),1721,hcl.r,hcl.g,hcl.b);
			draw_text("|",(VIRTW - 200),1718,hcl.r,hcl.g,hcl.b);
			draw_text((const char*)clip,(VIRTW - 170),1721,hcl.r,hcl.g,hcl.b); }
		}
	}
	glDisable(GL_BLEND);
	glPopMatrix();


	glColor3ub(hcl.r,hcl.g,hcl.b);
	switch(p->weaponsel->type)
	{
		case GUN_PISTOL:di(pistol,VIRTW-290,1500,280.0,0,0,1.0);break;
		case GUN_GRENADE:di(grenade,VIRTW-290,1500,280.0,0,0,1.0);break;
		case GUN_KNIFE:di(knife,VIRTW-290,1500,280.0,0,0,1.0);break;
		case GUN_SG552:di(sg552,VIRTW-290,1500,280.0,0,0,1.0);break;
		case GUN_M4A1:di(m4a1,VIRTW-290,1500,280.0,0,0,1.0);break;
		case GUN_FLARE:di(flare,VIRTW-290,1500,280.0,0,0,1.0);break;
		case GUN_ASSAULT:di(assault,VIRTW-290,1500,280.0,0,0,1.0);break;
		case GUN_SHOTGUN:di(shotgun,VIRTW-290,1500,280.0,0,0,1.0);break;
		case GUN_SNIPER:di(sniper,VIRTW-290,1500,280.0,0,0,1.0);break;
		case GUN_AKIMBO:di(pistol,VIRTW-290,1500,280.0,0,0,1.0);
	}
	glColor3ub(hcl.r,hcl.g,hcl.b);

	glPopMatrix();
	glFlush();

}

void HudMsg(const char *msg,int interval)
{
	m_stay = interval;
	strcpy(txt,msg);
}

void HudMsgHook()
{
	glColor3f(1.0,1.0,1.0);

	glPushMatrix();
	if(m_stay > 0)
	{
		glScalef(0.3,0.3,0.3);
		m_stay--; 
		draw_text(txt,30,2100,255,255,255,m_stay);
	}
	glPopMatrix();
}

void nvg()
{
	if(team_int(player1->team) != TEAM_RVSF)
	{
		return;
	}

	if(!nvgoggle)
	{
		nvgoggle = 1;
		playsound(105);
	} else {
		nvgoggle = 0;
	}
	addmsg(EF_NVG,"ri",nvgoggle);
}

void flash()
{
	if(team_int(player1->team) != TEAM_RVSF)
	{
		return;
	}

	if(!flashl)
	{
		flashl = 1;
		player1->flashlight = true;
	} else {
		flashl = 0;
		player1->flashlight = false;
	}

	addmsg(EF_FLASH,"ri",flashl);
	playsound(106);
}

void sethudcolor(int r,int g,int b)
{
	hcl.r = r;
	hcl.g = g;
	hcl.b = b;
}

COMMAND(sprkey,ARG_DOWN);
COMMAND(nvg,ARG_NONE);
COMMAND(flash,ARG_NONE);
COMMAND(sethudcolor,ARG_3INT);