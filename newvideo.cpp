#include "pch.h"
#include "cube.h"

float angle;// �hel rotace objektu
int next;// Pro animaci
int frame = 0;// Aktu�ln� sn�mek videa
int effect;// Zobrazen� objekt
bool env = TRUE;// Automaticky generovat texturov� koordin�ty?
bool bg = TRUE;// Zobrazovat pozad�?
bool sp;// Stisknut mezern�k?
bool ep;// Stisknuto E?
bool bp;// Stisknuto B?
AVISTREAMINFO psi;// Informace o datov�m proudu videa
PAVISTREAM pavi;// Handle proudu
PGETFRAME pgf;// Ukazatel na objekt GetFrame
BITMAPINFOHEADER bmih;// Hlavi�ka pro DrawDibDraw dek�dov�n�
long lastframe;// Posledn� sn�mek proudu
int width;// ���ka videa
int height;// V��ka videa
char* pdata;// Ukazatel na data textury
int mpf;// Doba zobrazen� jednoho sn�mku (Milliseconds Per Frame)
GLUquadricObj *quadratic;// Objekt quadraticu

HDRAWDIB hdd;// Handle DIBu
HBITMAP hBitmap;// Handle bitmapy z�visl� na za��zen�
HDC hdc = CreateCompatibleDC(0);// Kontext za��zen�
unsigned char* data = 0;// Ukazatel na bitmapu o zm�n�n� velikosti

void flipIt(void* buffer)// Prohod� �ervenou a modrou slo�ku pixel� v obr�zku
{
void* b = buffer;// Ukazatel na buffer
	__asm // ASM k�d
		{
			mov ecx, 256*256 // ��d�c� "prom�nn�" cyklu
			mov ebx, b // Ebx ukazuje na data
			label: // N�v�t� pro cyklus
			mov al, [ebx+0] // P�esune B slo�ku do al
			mov ah, [ebx+2] // P�esune R slo�ku do ah
			mov [ebx+2], al // Vlo�� B na spr�vnou pozici
			mov [ebx+0], ah // Vlo�� R na spr�vnou pozici
			add ebx, 3 // P�esun na dal�� t�i byty
			dec ecx // Dekrementuje ��ta�
			jnz label // Pokud se ��ta� nerovn� nule skok na n�v�t�
		}
}

void OpenAVI(LPCSTR szFile)// Otev�e AVI soubor
{
	TCHAR title[100];// Pro vyps�n� textu do titulku okna
	AVIFileInit();// P�iprav� knihovnu AVIFile na pou�it�

	if (AVIStreamOpenFromFile(&pavi, szFile, streamtypeVIDEO, 0, OF_READ, NULL) != 0)// Otev�e AVI proud
	{
		// Chybov� zpr�va
		MessageBox (HWND_DESKTOP, "Failed To Open The AVI Stream", "Error", MB_OK | MB_ICONEXCLAMATION);
	}