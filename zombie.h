#include "newhud.h"

#define BASEHP 500
#define random(max) (rand()%(max))

enum ZMModes {
	ZM_FIRSTZOMBIE = 0,
	ZM_ZOMBIEHORDE,
	ZM_NEMESIS
};

class ZMEvents {
 void OnRoundStart();
 void OnRoundEnd();
};

static char *ModeNames[] = { "First Zombie","Zombie Horde","Nemesis" };

extern int ZMMode;

extern void SelectRandomMode();

extern void CustomHUD(playerent *p);

extern float Energy;

extern bool insprint;

struct HUDColor {
	int r;
	int g;
	int b;
};
