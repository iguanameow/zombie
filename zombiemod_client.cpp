#include "pch.h"
#include "cube.h"

void Slay(int cn)
{
	addmsg(ADC_SLAY,"ri",cn);
}

void Slap(int cn,int amount)
{
	addmsg(ADC_SLAP,"rii",cn,amount);
}

void Blind(int cn)
{
	addmsg(ADC_BLIND,"ri",cn);
}

void Unblind(int cn)
{
	addmsg(ADC_UNBLIND,"ri",cn);
}

void MsgToAll(const char *msg)
{
	addmsg(ADC_MSGA,"rs",msg);
}

void MsgToClient(const char *cn, const char *msg)
{
	int i;
	i = atoi(cn);
	addmsg(ADC_MSGP,"ris",i,msg);
}

COMMANDN(slay,Slay,ARG_1INT);
COMMANDN(slap,Slap,ARG_2INT);
COMMANDN(blind,Blind,ARG_1INT);
COMMANDN(unblind,Unblind,ARG_1INT);
COMMANDN(msga,MsgToAll,ARG_1STR);
COMMANDN(msgc,MsgToClient,ARG_2STR);
//COMMANDN(song,StartMusic,ARG_5STR);